---
title: "Proton Compatibility"
category: Advice
order: 5
---
If after exploring all options you find that Linux support is still prohibitively difficult, time consuming, or expensive, there is always the option of making some attempt to remain compatible with Proton.

Proton is a Compatibility Tool created by Valve, that makes it possible for Linux gamers to play some Windows games on Linux with varying degrees of success. While this is an advantage particularly for games that release on Steam, it is not necessary for your game to be available on Steam to be played with Proton.

Generally speaking there is not much that you as a developer are required to do to take advantage of Proton. Proton compatibility when it occurs by accident, doesn't cost anything, and only expands the reach of your game to provide additional sales.

Statistically you are likely to find your game will already be compatible with Proton even without making an attempt to officially support it, as typically around 60% of Steam's games are compatible with Proton anyway.

Maintaining Proton compatibility is less a matter of taking specific steps, and more a list of technologies you should avoid to maintain compatibility with Proton, such as avoiding when possible:

* Intrusive DRM
* AntiCheat middleware
* Windows Media Foundation
* .Net Framework

For optional performance, it is recommended to make Vulkan your primary renderer *(or at least an option in your game's graphics settings)*.

[The official advice from Valve on this subject is:](https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561)

>*"We recommend you target Vulkan natively in order to offer the best possible performance on all platforms, or at least offer it as an option if possible. It's also a good idea to avoid any invasive third-party DRM middleware, as they sometimes prevent compatibility features from working as intended."*

The great advantage of Proton compatibility, is the expanded reach of your game to a slightly larger audience, without any additional burden of support. Small decisions during your game development on how to avoid losing compatibility with Proton early on, can help achieve 'Platinum' Proton compatibility, resulting in more sales.

The only catch, is that it is worth remembering the loyalty of many Linux gamers to their platform. Some Linux gamers will insist on only purchasing native Linux games for their platform, so called *'no tux, no bucks'* policy. To achieve the highest number of sales possible and take full advantage of the Linux market, a native Linux title is still necessary.
