---
title: Contribute
---

This guide is open source! [Fork us on Gitlab](https://gitlab.com/gradyvuckovic/linux-game-shipping-guide) and help contribute to the content of this guide, to expand it's content and improve the accuracy of the advice contained within.
